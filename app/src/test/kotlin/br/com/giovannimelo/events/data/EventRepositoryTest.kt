package br.com.giovannimelo.events.data

import br.com.giovannimelo.events.data.api.ApiService
import br.com.giovannimelo.events.data.repositories.EventRepository
import br.com.giovannimelo.events.data.repositories.Repository
import br.com.giovannimelo.events.models.Event
import br.com.giovannimelo.events.utils.Constants.EMAIL_CHECKIN
import br.com.giovannimelo.events.utils.Constants.EVENT_ID_CHECKIN
import br.com.giovannimelo.events.utils.Constants.NAME_CHECKIN
import com.google.gson.JsonObject
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class EventRepositoryTest {

    private lateinit var apiServiceMock: ApiService
    private lateinit var repository: Repository

    @Before
    fun setUp() {
        apiServiceMock = mock()
        repository = EventRepository(apiServiceMock)
    }

    @Test
    fun `Get events, when is requested, to retrieve a list of events, then returns a list of events`() =
        runBlocking {
            // Arrange
            val expectedList = Response.success(
                listOf(
                    Event(1, "Event 1"),
                    Event(2, "Event 2"),
                    Event(3, "Event 3"),
                    Event(4, "Event 4")
                )
            )
            whenever(apiServiceMock.getEventsAsync()).thenReturn(expectedList)

            // Act
            val actualList = repository.getEvents()

            // Assert
            Assert.assertEquals(expectedList, actualList)
        }

    @Test
    fun `Post check in, when is passed a json value, then return a json result`() = runBlocking {
        // Arrange
        val jsonBody = JsonObject().also {
            it.addProperty(EVENT_ID_CHECKIN, 1)
            it.addProperty(NAME_CHECKIN, "Test")
            it.addProperty(EMAIL_CHECKIN, "mail@to.me")
        }

        val expectedJsonResponse = Response.success(JsonObject().also {
            it.addProperty("code", "200")
        })
        whenever(apiServiceMock.postCheckInAsync(jsonBody)).thenReturn(expectedJsonResponse)

        // Act
        val actualResult = repository.postCheckIn(jsonBody)

        // Assert
        Assert.assertEquals(expectedJsonResponse, actualResult)
    }
}