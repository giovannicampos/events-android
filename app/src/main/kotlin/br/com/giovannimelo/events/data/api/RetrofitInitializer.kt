package br.com.giovannimelo.events.data.api

import br.com.giovannimelo.events.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitInitializer {

    private const val RETROFIT_TIMEOUT = 30L

    private fun logInterceptor() = HttpLoggingInterceptor()
        .apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }

    private fun createOkHttpClient() = OkHttpClient.Builder()
        .addInterceptor(logInterceptor())
        .readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
        .connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
        .build()

    private fun createNetworkClient() = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(createOkHttpClient())
        .build()

    fun apiService(): ApiService = createNetworkClient().create(
        ApiService::class.java)
}