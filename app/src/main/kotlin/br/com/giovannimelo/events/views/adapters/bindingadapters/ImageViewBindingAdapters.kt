package br.com.giovannimelo.events.views.adapters.bindingadapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import br.com.giovannimelo.events.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("android:src")
fun ImageView.imageFromUrl(imageUrl: String?) {
    Glide.with(context)
        .load(imageUrl)
        .apply(
            RequestOptions()
                .dontTransform()
                .error(R.drawable.layer_placeholder)
        )
        .placeholder(R.drawable.layer_placeholder)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

@BindingAdapter("imageRoundedFromUrl")
fun ImageView.bindImageRoundedFromUrl(imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .dontTransform()
                    .error(R.drawable.layer_placeholder)
            )
            .transform(CenterCrop(), RoundedCorners(250))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
    }
}