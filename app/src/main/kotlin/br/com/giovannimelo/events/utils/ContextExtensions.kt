package br.com.giovannimelo.events.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import androidx.appcompat.app.AlertDialog
import br.com.giovannimelo.events.R

fun Context.isConnected(): Boolean {
    val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val network: Network? = connMgr.activeNetwork
    val networkCapabilities: NetworkCapabilities? = connMgr.getNetworkCapabilities(network)
    networkCapabilities?.let {
        return (it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
    } ?: run {
        return false
    }
}

fun Context.showDialogMessage(message: String) {
    val dialog = AlertDialog.Builder(this, R.style.AppTheme_AlertDialog)
        .setTitle(getString(R.string.app_name))
        .setMessage(message)
        .setPositiveButton(android.R.string.yes) { dialogInterface, _ ->
            dialogInterface.dismiss()
        }.create()
    dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
    dialog.show()
}
