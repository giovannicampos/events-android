package br.com.giovannimelo.events.utils

import android.view.animation.AnimationUtils.loadLayoutAnimation
import androidx.recyclerview.widget.RecyclerView
import br.com.giovannimelo.events.R

fun RecyclerView.runLayoutAnimation() {
    val controller = loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

    layoutAnimation = controller
    adapter?.notifyDataSetChanged()
    scheduleLayoutAnimation()
}