package br.com.giovannimelo.events.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Event(
    val id: Int = 0,
    val title: String = "",
    val price: Double = 0.0,
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val image: String = "",
    val description: String = "",
    val date: Long = 0,
    val people: List<People> = listOf(),
    val cupons: List<Cupon> = listOf()
) : Parcelable