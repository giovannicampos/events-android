package br.com.giovannimelo.events.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import br.com.giovannimelo.events.databinding.ItemEventBinding
import br.com.giovannimelo.events.models.Event

class EventAdapter(
    private var listItems: List<Event>,
    private val listener: (AppCompatImageView, Event) -> Unit
) : RecyclerView.Adapter<EventAdapter.EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder =
        EventViewHolder(
            ItemEventBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount(): Int = listItems.size

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(listItems[position], listener)
    }

    fun submitList(items: List<Event>) {
        listItems = items
        notifyDataSetChanged()
    }

    class EventViewHolder(private val binding: ItemEventBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(event: Event, listener: (AppCompatImageView, Event) -> Unit) {
            binding.event = event
            binding.clickListener = View.OnClickListener { listener(binding.ivThumbEvent, event) }
            binding.executePendingBindings()
        }
    }
}