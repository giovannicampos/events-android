package br.com.giovannimelo.events.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.giovannimelo.events.databinding.ItemParticipantBinding
import br.com.giovannimelo.events.models.People

class ParticipantAdapter(private var listItems: List<People>) :
    RecyclerView.Adapter<ParticipantAdapter.ParticipantViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParticipantViewHolder =
        ParticipantViewHolder(
            ItemParticipantBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount(): Int = listItems.size

    override fun onBindViewHolder(holder: ParticipantViewHolder, position: Int) {
        holder.bind(listItems[position])
    }

    fun submitList(items: List<People>) {
        listItems = items
        notifyDataSetChanged()
    }

    class ParticipantViewHolder(private val binding: ItemParticipantBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(participant: People) {
            binding.participant = participant
            binding.executePendingBindings()
        }
    }
}