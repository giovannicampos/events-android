package br.com.giovannimelo.events.views.adapters.bindingadapters

import android.location.Address
import android.location.Geocoder
import android.text.format.DateUtils
import android.widget.TextView
import androidx.databinding.BindingAdapter
import br.com.giovannimelo.events.R
import br.com.giovannimelo.events.utils.Constants.BRAZILIAN_PRICE_PATTERN
import br.com.giovannimelo.events.utils.Constants.LOCALE_BRAZIL
import br.com.giovannimelo.events.utils.Constants.STATES
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

@BindingAdapter("formatPrice")
fun TextView.bindFormatPrice(price: Double) {
    val decimalFormat = DecimalFormat(
        BRAZILIAN_PRICE_PATTERN,
        DecimalFormatSymbols(Locale("pt", "BR"))
    )

    text = this.context.getString(
        R.string.currency_with_space,
        decimalFormat.format(price)
    )
}

@BindingAdapter("formatDate")
fun TextView.bindFormatDate(timestamp: Long) {
    text = DateUtils.formatDateTime(context, timestamp, 0)
}

@BindingAdapter("formatWeekday")
fun TextView.bindFormatWeekday(timestamp: Long) {
    text = DateUtils.formatDateTime(
        context,
        timestamp,
        DateUtils.FORMAT_SHOW_WEEKDAY or DateUtils.FORMAT_ABBREV_WEEKDAY
    ).toUpperCase(LOCALE_BRAZIL)
}

@BindingAdapter("formatMonth")
fun TextView.bindFormatMonth(timestamp: Long) {
    val date = DateUtils.formatDateTime(
        context,
        timestamp,
        DateUtils.FORMAT_NO_MONTH_DAY or DateUtils.FORMAT_ABBREV_MONTH
    ).toUpperCase(LOCALE_BRAZIL)
    text = date.split(" ")[0]
}

@BindingAdapter("formatDay")
fun TextView.bindFormatDay(timestamp: Long) {
    val date = DateUtils.formatDateTime(
        context,
        timestamp,
        DateUtils.FORMAT_NO_YEAR
    )
    text = date.split(" ")[0]
}

@BindingAdapter(value = ["latCity", "longCity"], requireAll = true)
fun TextView.bindFormatCityAndState(latitude: Double, longitude: Double) {
    val addresses: List<Address> = Geocoder(context, LOCALE_BRAZIL).getFromLocation(
        latitude,
        longitude,
        1
    )

    val address = addresses.first()
    val city = address.subAdminArea
    val state = address.adminArea
    text = context.getString(R.string.city_and_state, city, STATES[state])
}

@BindingAdapter(value = ["latLocation", "longLocation"], requireAll = true)
fun TextView.bindFormatLocation(latitude: Double, longitude: Double) {
    val addresses: List<Address> = Geocoder(context, LOCALE_BRAZIL).getFromLocation(
        latitude,
        longitude,
        1
    )
    val address = addresses.first()
    text = address.getAddressLine(address.maxAddressLineIndex)
}