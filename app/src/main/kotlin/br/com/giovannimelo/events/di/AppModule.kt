package br.com.giovannimelo.events.di

import br.com.giovannimelo.events.data.api.RetrofitInitializer
import br.com.giovannimelo.events.data.repositories.EventRepository
import br.com.giovannimelo.events.data.repositories.Repository
import br.com.giovannimelo.events.viewmodels.EventViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun getAppModules() = module {
    factory { RetrofitInitializer.apiService() }
    single<Repository> { EventRepository(get()) }
    viewModel { EventViewModel(get()) }
}