package br.com.giovannimelo.events.views.activities

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import br.com.giovannimelo.events.utils.activityBinding
import br.com.giovannimelo.events.viewmodels.EventViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class BaseBindingActivity<T : ViewDataBinding>(@LayoutRes val resId: Int) :
    AppCompatActivity() {

    val binding by activityBinding<T>(resId)
    val viewModel by viewModel<EventViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.lifecycleOwner = this

        subscribeUi()
    }

    abstract fun subscribeUi()
}