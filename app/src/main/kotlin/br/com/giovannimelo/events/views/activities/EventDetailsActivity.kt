package br.com.giovannimelo.events.views.activities

import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Patterns
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import br.com.giovannimelo.events.R
import br.com.giovannimelo.events.databinding.ActivityEventDetailsBinding
import br.com.giovannimelo.events.databinding.LayoutCheckinEventBinding
import br.com.giovannimelo.events.models.Event
import br.com.giovannimelo.events.utils.Constants
import br.com.giovannimelo.events.utils.animateTransitionOnRebind
import br.com.giovannimelo.events.utils.showDialogMessage
import br.com.giovannimelo.events.views.adapters.ParticipantAdapter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.card.MaterialCardView

class EventDetailsActivity :
    BaseBindingActivity<ActivityEventDetailsBinding>(R.layout.activity_event_details) {

    private val eventExtra by lazy { intent.getParcelableExtra(EVENT_ARG) ?: Event() }
    private val participantAdapter = ParticipantAdapter(emptyList())

    private lateinit var behavior: BottomSheetBehavior<MaterialCardView>

    companion object {
        const val EVENT_ARG = "event_arg"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initBindings()
        configureToolbar()
        configureBottomSheet()
        configureRecyclerView()
        initMaps()
    }

    override fun subscribeUi() {
        participantAdapter.submitList(eventExtra.people)

        viewModel.message.observe(this, Observer {
            showDialogMessage(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(
            R.menu.menu_actions,
            menu
        )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.action_share -> {
            shareContent(
                StringBuilder()
                    .append(getString(R.string.shared_content)).append("\n\n")
                    .append(
                        getString(
                            R.string.uri_maps,
                            eventExtra.latitude.toString(),
                            eventExtra.longitude.toString()
                        )
                    )
                    .toString()
            )
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        behavior.isHideable = true
        behavior.state = BottomSheetBehavior.STATE_HIDDEN
        super.onBackPressed()
    }

    private fun shareContent(content: String) {
        val shareIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, content)
            type = "text/plain"
        }
        startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.share)))
    }

    private fun configureToolbar() {
        setSupportActionBar(binding.appbar.toolbarApp)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
    }

    private fun initMaps() {
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentMap) as SupportMapFragment

        val addresses: List<Address> = Geocoder(this, Constants.LOCALE_BRAZIL).getFromLocation(
            eventExtra.latitude,
            eventExtra.longitude,
            1
        )

        mapFragment.getMapAsync { map ->
            val location = LatLng(eventExtra.latitude, eventExtra.longitude)
            map.addMarker(
                MarkerOptions().position(location)
                    .snippet(getString(R.string.neighborhood, addresses[0].subLocality))
                    .title(eventExtra.title)
            )
            map.animateCamera(CameraUpdateFactory.zoomTo(15.0f))
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15.0f))
        }
    }

    private fun initBindings() {
        binding.event = eventExtra
        binding.clickListener = View.OnClickListener {
            showCheckInDialog()
        }
        binding.animateTransitionOnRebind()
        binding.executePendingBindings()
    }

    private fun showCheckInDialog() {
        val builder = AlertDialog.Builder(this)
        var dialog = builder.create()

        val viewBinding = LayoutCheckinEventBinding.inflate(
            LayoutInflater.from(this),
            null,
            true
        )

        val viewBindingApplied = viewBinding.apply {
            etName.doAfterTextChanged {
                nameValid = !it.isNullOrEmpty()
            }
            etEmail.doAfterTextChanged {
                emailValid =
                    !it.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(it.toString()).matches()
            }
            clickListener = View.OnClickListener {
                viewModel.doCheckIn(etName.text.toString(), etEmail.text.toString(), eventExtra.id)
                dialog.dismiss()
            }
        }

        builder.setView(viewBindingApplied.root)
        dialog = builder.create()
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        dialog.show()
    }

    private fun configureBottomSheet() {
        behavior = BottomSheetBehavior.from(binding.bottomSheet)
        behavior.isHideable = false
        val layoutParams = binding.bottomSheet.layoutParams
        val displayMetrics = DisplayMetrics().also {
            windowManager.defaultDisplay.getMetrics(it)
        }
        layoutParams.height = displayMetrics.heightPixels
        binding.bottomSheet.layoutParams = layoutParams
    }

    private fun configureRecyclerView() {
        binding.content.rvParticipants.apply {
            adapter = participantAdapter
            setHasFixedSize(true)
        }
    }
}