package br.com.giovannimelo.events.data.api

import br.com.giovannimelo.events.models.Event
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @GET("events")
    suspend fun getEventsAsync(): Response<List<Event>>

    @POST("checkin")
    suspend fun postCheckInAsync(@Body requestBody: JsonObject): Response<JsonObject>
}