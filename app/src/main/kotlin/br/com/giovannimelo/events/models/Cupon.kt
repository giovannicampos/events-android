package br.com.giovannimelo.events.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Cupon(
    val id: String = "",
    val eventId: String = "",
    val discount: Int = 0
) : Parcelable