package br.com.giovannimelo.events.views.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.lifecycle.Observer
import br.com.giovannimelo.events.R
import br.com.giovannimelo.events.databinding.ActivityEventsBinding
import br.com.giovannimelo.events.utils.animateTransitionOnRebind
import br.com.giovannimelo.events.utils.isConnected
import br.com.giovannimelo.events.utils.runLayoutAnimation
import br.com.giovannimelo.events.utils.showDialogMessage
import br.com.giovannimelo.events.views.adapters.EventAdapter

class EventsActivity : BaseBindingActivity<ActivityEventsBinding>(R.layout.activity_events) {

    private val adapter = EventAdapter(emptyList()) { image, event ->
        val intent = Intent(this, EventDetailsActivity::class.java).also {
            it.putExtra(EventDetailsActivity.EVENT_ARG, event)
        }
        val pair: Pair<View, String> = Pair(image, getString(R.string.transition_name_image))
        startActivity(
            intent,
            ActivityOptionsCompat.makeSceneTransitionAnimation(this, pair).toBundle()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initBindings()
        configureSwipe()
        loadEvents()
    }

    override fun subscribeUi() {
        viewModel.events.observe(this, Observer {
            if (it != null) {
                adapter.submitList(it)
                binding.swipeEvents.isRefreshing = false
                binding.isLoading = false
                binding.rvEvents.runLayoutAnimation()
            }
        })

        viewModel.message.observe(this, Observer {
            showDialogMessage(it)
            binding.isLoading = false
            binding.swipeEvents.isRefreshing = false
        })
    }

    private fun initBindings() {
        binding.rvEvents.adapter = adapter
        binding.animateTransitionOnRebind()
        binding.executePendingBindings()
        binding.btTryAgain.setOnClickListener {
            loadEvents()
        }
    }

    private fun configureSwipe() {
        binding.swipeEvents.apply {
            setColorSchemeColors(
                ContextCompat.getColor(this@EventsActivity, R.color.colorAccent)
            )
            setProgressBackgroundColorSchemeColor(
                ContextCompat.getColor(this@EventsActivity, R.color.black_gray)
            )
            setOnRefreshListener {
                adapter.submitList(emptyList())
                loadEvents()
            }
        }
    }

    private fun loadEvents() {
        binding.isLoading = isConnected()
        binding.isConnected = isConnected()
        binding.swipeEvents.isRefreshing = !binding.isLoading

        if (binding.isConnected)
            viewModel.getEvents()
    }
}
