package br.com.giovannimelo.events.data.repositories

import br.com.giovannimelo.events.models.Event
import com.google.gson.JsonObject
import retrofit2.Response

interface Repository {

    suspend fun getEvents(): Response<List<Event>>
    suspend fun postCheckIn(json: JsonObject): Response<JsonObject>
}