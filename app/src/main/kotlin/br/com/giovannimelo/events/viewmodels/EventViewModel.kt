package br.com.giovannimelo.events.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import br.com.giovannimelo.events.data.repositories.Repository
import br.com.giovannimelo.events.models.Event
import br.com.giovannimelo.events.utils.Constants.EMAIL_CHECKIN
import br.com.giovannimelo.events.utils.Constants.EVENT_ID_CHECKIN
import br.com.giovannimelo.events.utils.Constants.NAME_CHECKIN
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Response

class EventViewModel(private val repository: Repository) : BaseViewModel() {

    companion object {
        private const val CHECKIN_SUCCESS_MESSAGE = "Check-in realizado com sucesso"
        private const val MESSAGE_EXCEPTION = "Ocorreu um erro ao recuperar os dados do servidor."
        private const val UNKNOWN_ERROR = "Ocorreu um erro inesperado."
    }

    private val _events = MutableLiveData<List<Event>>()
    private val _message = MutableLiveData<String>()

    val events = Transformations.map(_events) { it }
    val message = Transformations.map(_message) { it }

    fun getEvents() = launch {
        delay(1_000)

        try {
            val response: Response<List<Event>> = repository.getEvents()

            when (response.code()) {
                200 -> {
                    _events.postValue(response.body())
                }
                400, 404, 500 -> {
                    _message.postValue(response.errorBody()?.string())
                }
                else -> {
                    _message.postValue("$UNKNOWN_ERROR - error code: ${response.code()}")
                }
            }
        } catch (e: Exception) {
            when (e) {
                is JsonSyntaxException,
                is NumberFormatException -> {
                    _message.postValue(MESSAGE_EXCEPTION)
                }
                else -> {
                    _message.postValue(e.message)
                }
            }
            e.printStackTrace()
        }
    }

    fun doCheckIn(name: String, email: String, id: Int) = launch {
        val json = JsonObject().also {
            it.addProperty(EVENT_ID_CHECKIN, id)
            it.addProperty(NAME_CHECKIN, name)
            it.addProperty(EMAIL_CHECKIN, email)
        }

        delay(1_000)

        try {
            val response: Response<JsonObject> = repository.postCheckIn(json)
            when (response.code()) {
                200, 201 -> {
                    _message.postValue(CHECKIN_SUCCESS_MESSAGE)
                }
                400, 404, 500 -> {
                    _message.postValue(response.errorBody()?.string())
                }
                else -> {
                    _message.postValue("$UNKNOWN_ERROR - error code: ${response.code()}")
                }
            }
        } catch (e: Exception) {
            _message.postValue(e.message)
            e.printStackTrace()
        }
    }
}