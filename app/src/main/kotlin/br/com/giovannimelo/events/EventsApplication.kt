package br.com.giovannimelo.events

import android.app.Application
import br.com.giovannimelo.events.di.getAppModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EventsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@EventsApplication)
            modules(getAppModules())
        }
    }
}