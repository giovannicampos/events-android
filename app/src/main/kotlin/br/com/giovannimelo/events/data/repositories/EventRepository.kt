package br.com.giovannimelo.events.data.repositories

import br.com.giovannimelo.events.data.api.ApiService
import br.com.giovannimelo.events.models.Event
import com.google.gson.JsonObject
import retrofit2.Response

class EventRepository(private val apiService: ApiService) : Repository {

    override suspend fun getEvents(): Response<List<Event>> =
        apiService.getEventsAsync()

    override suspend fun postCheckIn(json: JsonObject): Response<JsonObject> =
        apiService.postCheckInAsync(json)
}