[![Kotlin](https://img.shields.io/badge/version-1.0.0-orange.svg)]()
[![Kotlin](https://img.shields.io/badge/kotlin-powered-green.svg)]()
[![ktlint](https://img.shields.io/badge/code%20style-%E2%9D%A4-FF4081.svg)](https://ktlint.github.io/)

# Events
A simple application that show a list of events, with details screen and check-in action.

## Architecture used on this project:

- MVVM: [developer.android.com](https://developer.android.com/jetpack/docs/guide#recommended-app-arch "developer.android.com")
- According to Google, MVVM is recomended because it is present on Android Architectural Components, on [Android Jetpack](https://developer.android.com/jetpack "Android Jetpack").

## Libraries used on this project:

### Coroutines:
- Version: 1.1.1
- Used: To run tasks in background

### Dynamic Animation:
- Version: 1.0.0
- Used: To make views animations automatically

### Google Maps:
- Version: 17.0.0
- Used: To show location from latitudes and longitudes

### Glide:
- Version: 4.9.0
- Used: To load images from URI

### Koin:
- Version: 2.0.1
- Used: Dependence injection

### Ktlint:
- Version: 0.30.0
- Used: To format and clean code

### Lottie:
- Version: 3.0.7
- Used: To animate loaders

### Mockito Kotlin:
- Version: 2.1.0
- Used: To mock objects in unit tests

### OkHttp:
- Version: 4.0.1
- Used: With Retrofit to intercept Logs and create requests clients

### Retrofit:
- Version: 2.6.0
- Used: To make requests on server API

